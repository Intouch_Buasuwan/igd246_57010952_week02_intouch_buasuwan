﻿using UnityEngine;
using System.Collections;

public class FantasyMonsterController : MonoBehaviour {

	public Animator FantasyMonsterAnimator;
	public int FantasyMonsterHp;

	void Start () {
		FantasyMonsterHp = 100;
	}

	void Update () {
		if (Input.GetKey (KeyCode.UpArrow)) {
			FantasyMonsterAnimator.SetFloat ("Speed", 1);
		} else {
			FantasyMonsterAnimator.SetFloat ("Speed", 0);
		}
		
		if (Input.GetKey (KeyCode.LeftShift)) {
			FantasyMonsterAnimator.SetBool ("IsSprint", true);
		} else {
			FantasyMonsterAnimator.SetBool ("IsSprint", false);
		}
		
		if (Input.GetKeyDown (KeyCode.Space)) {
			FantasyMonsterAnimator.SetTrigger ("Jump");
		}

		if (Input.GetKeyDown (KeyCode.Q)) {
			FantasyMonsterAnimator.SetTrigger ("Attack");
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			FantasyMonsterAnimator.SetTrigger ("Skill");
		}

		if (Input.GetKeyDown (KeyCode.E)) {
			FantasyMonsterAnimator.SetTrigger ("Hit");
			FantasyMonsterHp -= 20;
		}

		if (FantasyMonsterHp <= 0) {
			FantasyMonsterAnimator.SetTrigger ("Dead");
		}

		FantasyMonsterAnimator.SetInteger ("Hp", FantasyMonsterHp);
	}
}
