﻿using UnityEngine;
using System.Collections;

namespace IGD246.Week02 {

    public class SkeletonController : MonoBehaviour {

		public Animator SkeletonAnimator;
		public int SkeletonHP;

        void Start() {
			SkeletonHP = 100;
        }
		
        void Update() {
			if (Input.GetKey (KeyCode.W)) {
				SkeletonAnimator.SetFloat ("Speed", 1);
			} else if (Input.GetKey (KeyCode.S)) {
				SkeletonAnimator.SetFloat ("Speed", -1);
			} else {
				SkeletonAnimator.SetFloat ("Speed", 0);
			}

			if (Input.GetKey (KeyCode.LeftShift)) {
				SkeletonAnimator.SetBool ("IsSprint", true);
			} else {
				SkeletonAnimator.SetBool ("IsSprint", false);
			}

			if (Input.GetKeyDown (KeyCode.K)) {
				SkeletonHP -= 20;
				SkeletonAnimator.SetTrigger ("HitTrigger");
			}

			SkeletonAnimator.SetInteger ("Hp", SkeletonHP);

			if (Input.GetKeyDown (KeyCode.F)) {
				SkeletonAnimator.SetTrigger ("AttackTrigger");
			}

			if (Input.GetKeyDown (KeyCode.Space)) {
				SkeletonAnimator.SetTrigger ("JumpTrigger");
			}
        }
    }

}