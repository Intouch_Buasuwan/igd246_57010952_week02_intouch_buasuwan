﻿using UnityEngine;
using System.Collections;

public class AmandaController : MonoBehaviour {

	public Animator AmandaAnimator;
	
	void Start() {
	}
	
	void Update() {
		if (Input.GetKey (KeyCode.UpArrow)) {
			AmandaAnimator.SetFloat ("speed", 1);
		} else {
			AmandaAnimator.SetFloat ("speed", 0);
		}

		if (Input.GetKey (KeyCode.LeftControl)) {
			AmandaAnimator.SetBool ("crouch", true);
		} else {
			AmandaAnimator.SetBool ("crouch", false);
		}
		
		if (Input.GetKey (KeyCode.LeftShift)) {
			AmandaAnimator.SetBool ("sprint", true);
		} else {
			AmandaAnimator.SetBool ("sprint", false);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			AmandaAnimator.SetTrigger ("jump");
		}
	}
}
